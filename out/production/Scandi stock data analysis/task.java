public class  Main
{
    public static void main(String[] args)  //static method
    {
        System.out.println("Static method");
    }
}




////package iterle;
//
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.util.Scanner;
//
//public class Main
//{
//    //Delimiters used in the CSV file
//    private static final String COMMA_DELIMITER = ",";
//
//    public static void Main(String args[])
//    {
//
//        Scanner scanner = null;
//        try {
//            //Get the scanner instance
//            scanner = new Scanner(new File("Employee.csv"));
//            //Use Delimiter as COMMA
//            scanner.useDelimiter(COMMA_DELIMITER);
//            while(scanner.hasNext())
//            {
//                System.out.print(scanner.next()+"   ");
//            }
//        }
//        catch (FileNotFoundException fe)
//        {
//            fe.printStackTrace();
//        }
//        finally
//        {
//            scanner.close();
//        }
//    }
//}


//    FileInputStream inputStream = null;
//    Scanner sc = null;
//try {
//        inputStream = new FileInputStream(path);
//        sc = new Scanner(inputStream, "UTF-8");
//        while (sc.hasNextLine()) {
//            String line = sc.nextLine();
//            // System.out.println(line);
//        }
//        // note that Scanner suppresses exceptions
//        if (sc.ioException() != null) {
//            throw sc.ioException();
//        }
//} finally {
//        if (inputStream != null) {
//            inputStream.close();
//        }
//        if (sc != null) {
//            sc.close();
//        }
//}