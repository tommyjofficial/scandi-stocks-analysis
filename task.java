package com.csv_task;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class task {



    private static final String comma = ",";

    public static void main(String args[])
    {


        // GETS ALL STOCK NAMES FROM CSV!
        Scanner scanner = null;

        ArrayList<String> stocks = new ArrayList<String>();
        try {

            scanner = new Scanner(new File("scandi.csv"));
            String line= "";

            scanner.useDelimiter(comma);
            while(scanner.hasNext())
            {

                line = scanner.nextLine();
                String[] values= line.split(comma);
                stocks.add(values[0]);
//                System.out.println(values[0]);
            }
        }
        catch (FileNotFoundException fe)
        {
            fe.printStackTrace();
        }
        finally
        {
            scanner.close();
        }

        Set<String> stock_names = new HashSet<String>(stocks);
//        System.out.println(stock_names);






        Double[] sorted_arr;
        double trade_times_mean;
        double trade_times_median;
        double bid_times_mean;
        double bid_times_median;
        double ask_times_mean;
        double ask_times_median;
        Double trade_times_max;
        Double bid_times_max;
        Double ask_times_max;
        double spreads_mean;
        double spreads_median;
        double trades_round;
        double vol_round;

        // ITERATES CVS FILE FOR EACH STOCK AND DOES REQUIRED CALCULATIONS
        Iterator<String> it = stock_names.iterator();
        while (it.hasNext()) {

            String stock_name=it.next();

//            Set<Integer> trades_times= new HashSet<Integer>();
            List<Double> trades_times= new ArrayList<>();
            Set<Double> bid_times=new HashSet<Double>();
            Set<Double> ask_times=new HashSet<Double>();
            int trades_rounds=0;
            int volume_rounds=0;
            double prev_trade_time=-1;
            double prev_bid_time=-1;
            double prev_ask_time= -1;
            Set<Double> spreads=new HashSet<Double>();
            Set<Double> trades_price=new HashSet<Double>();
            String prev_date= "";


            try {

                scanner = new Scanner(new File("scandi.csv"));
                String line = "";

                scanner.useDelimiter(comma);
                while (scanner.hasNext())
                {

                    line = scanner.nextLine();
                    String[] values = line.split(comma);
                    // String[] "values" values :
                    // 0 = Bloomberg Code/Stock identifier
                    // 2 = Bid Price
                    // 3 = Ask Price
                    // 4 = Trade Price
                    // 5 = Bid Volume
                    // 6 = Ask Volume
                    // 7 = Trade Volume
                    // 8 = Update type => 1=Trade; 2= Change to Bid (Px or Vol); 3=Change to Ask (Px or Vol)
                    // 10 = Date
                    // 11 = Time in seconds past midnight
                    // 12 = Opening price
                    // 14 = Condition codes

//                    System.out.println(((values[0] == stock_name) & ((values[14] == "XT") | (values[14] == "") & (Double.parseDouble(values[3])) > Double.parseDouble(values[4])) & (values[10].equals(prev_date))));
                    //   stock identifier            condition code XT      condition code none       bid price > ask price (not an auction)                       if it's a new date don't add time
                    if ((values[0].equals(stock_name)) & ((values[14].equals("XT")) | (values[14].equals("")) & (Double.parseDouble(values[3])) > Double.parseDouble(values[4])) & (values[10].equals(prev_date))) {


                        if (Integer.parseInt(values[8]) == 1) { //Update type 1

                            if (prev_trade_time != -1) {
                                trades_times.add(Double.parseDouble(values[11]) - prev_trade_time); //time - previous trade time

                                trades_price.add(Double.parseDouble(values[4]));
                                spreads.add(Double.parseDouble(values[3]) - Double.parseDouble(values[4])); //calculate spread
                            }
                            prev_trade_time = Double.parseDouble(values[11]);

                            if (Double.parseDouble(values[4]) % 10 == 0) {
                                trades_rounds += 1;
                            }
                            if (Integer.parseInt(values[7]) % 10 == 0) {
                                volume_rounds += 1;
                            }
                        }

                        if (Integer.parseInt(values[8]) == 2) { //Update type 2
                            if (prev_bid_time != -1) {
                                bid_times.add((Double.parseDouble(values[11]) - prev_bid_time)); //time - previous bid time
                            }
                            prev_bid_time = Double.parseDouble(values[11]);
                        }
                        if (Integer.parseInt(values[8]) == 3) { //Update type 3
                            if (prev_ask_time != -1) {
                                bid_times.add((Double.parseDouble(values[11]) - prev_ask_time)); //time - previous ask time
                            }
                            prev_ask_time = Double.parseDouble(values[11]);

                        }
                    }
                    prev_date=values[10]; //date
                }
            }
            catch (FileNotFoundException fe)
            {
                fe.printStackTrace();
            }
            finally
            {
                scanner.close();
            }

            if (trades_times.size()>0) {
                trade_times_mean = getAverage(trades_times.toArray(new Double[trades_times.size()]));
                sorted_arr = trades_times.toArray(new Double[0]);
                Arrays.sort(sorted_arr);
                trade_times_median = getMedian(sorted_arr);
                trade_times_max = Collections.max(trades_times);
            }else{
                trade_times_mean=Double.NaN;
                trade_times_median=Double.NaN;
                trade_times_max = Double.NaN;
            }

            if (bid_times.size()>0) {
                bid_times_mean = getAverage(bid_times.toArray(new Double[bid_times.size()]));
                sorted_arr = bid_times.toArray(new Double[0]);
                Arrays.sort(sorted_arr);
                bid_times_median = getMedian(sorted_arr);
                bid_times_max = Collections.max(bid_times);
            }else{
                bid_times_mean =Double.NaN;
                bid_times_median =Double.NaN;
                bid_times_max = Double.NaN;
            }

            if (ask_times.size()>0) {
                ask_times_mean = getAverage(ask_times.toArray(new Double[ask_times.size()]));
                sorted_arr = ask_times.toArray(new Double[0]);
                Arrays.sort(sorted_arr);
                ask_times_median = getMedian(sorted_arr);
                ask_times_max = Collections.max(ask_times);
            }else{
                ask_times_mean =Double.NaN;
                ask_times_median =Double.NaN;
                ask_times_max = Double.NaN;
            }


            if (spreads.size()>0) {
                spreads_mean = getAverage(spreads.toArray(new Double[spreads.size()]));
                sorted_arr = spreads.toArray(new Double[0]);
                Arrays.sort(sorted_arr);
                spreads_median = getMedian(sorted_arr);
            }else{
                spreads_mean=Double.NaN;
                spreads_median=Double.NaN;
            }

            if (trades_price.size()>0) {
                trades_round = trades_rounds / trades_price.size();
                vol_round = volume_rounds / trades_price.size();
            }else{
                trades_round = Double.NaN;
                vol_round = Double.NaN;
            }


            System.out.println("Stock"+"\t"+"Mean time trades (s)"+"\t"+"Median time trades (s)"+"\t"+"Mean time bid (s)"+"\t"+"Median time bid (s)"+"\t"+"Mean time ask (s)"+"\t"+"Median time ask (s)"+"\t"+"Longest time trade (s)"+"\t"+"Longest time bid (s)"+"\t"+"Longest time ask"+"\t"+"Mean spread"+"\t"+"Median spread"+"\t"+"Round number probability price (%)"+"\t"+"Round number probability volume (%)");
            System.out.println(stock_name+"\t"+trade_times_mean+"\t"+trade_times_median+"\t"+bid_times_mean+"\t"+bid_times_median+"\t"+ask_times_mean+"\t"+ask_times_median+"\t"+trade_times_max+"\t"+bid_times_max+"\t"+ask_times_max+"\t"+spreads_mean+"\t"+spreads_median+"\t"+trades_round+"\t"+vol_round);

        }



    }

    public static double getAverage(Double[] array) {
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum / array.length;
    }

    public static double getMedian(Double[] array) {
        int middle = array.length/2;
        if (array.length%2 == 1) {
            return array[middle];
        } else {
            return (array[middle-1] + array[middle]) / 2.0;
        }
    }

}
