## Analysis of 100 Scandinavian blue chip stocks (Apr 2015)

### Following calculations were made on a stock by stock basis :

* Mean time between trades
* Median time between trades
* Mean time between tick changes
* Median time between tick changes
* Longest time between trades
* Longest time between tick changes
* Mean bid ask spread
* Median bid ask spread
* Examples of the round number effect - (both in traded values and traded volumes)

CVS file of AWS 4 days' tick data for 100 Scandinavian blue chip stocks. The CSV has the following columns:

1 = Bloomberg Code/Stock identifier
3 = Bid Price
4 = Ask Price
5 = Trade Price
6 = Bid Volume
7 = Ask Volume
8 = Trade Volume
9 = Update type => 1=Trade; 2= Change to Bid (Px or Vol); 3=Change to Ask (Px or Vol)
11 = Date
12 = Time in seconds past midnight
13 = Opening price
15 = Condition codes

This data is over several days and so when no trading occurs there are large time gaps to take into account. Auctions are excluded from the analysis. There are 2 auctions a day - morning and afternoon. During this period spreads are crossed along with specific condition codes.

### Results

Solution presented here is is both: Python and Java. One of the stock's data ("BBHBEAT") has some data missing, compared to the rest in the CSV file. Tick changes were calculated by mean and median time between changes for both: bid and ask prices. For "round number effect" percentage of round numbers in the dataset was calculated. The date changes for the first time (new day) were ignored, to exclude time gaps when the market is closed.
