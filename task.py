#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import time
from datetime import datetime, timedelta
import pandas as pandas
import modin.pandas as pd
import numpy as np
import statistics as st


# In[28]:


stock_names=[]
url='scandi.csv'
for chunk in pd.read_csv(url,engine='python',header=None, chunksize=500000, usecols=[0,2,3,4,7,8,10,11,14,15], skip_blank_lines=False) :
    chunk.columns = ['stock name','bid','ask','trade','trade vol','update','date','time','code' ]
    
    stock_names= np.append(stock_names ,chunk['stock name'].unique()) 
    stock_names= np.unique(stock_names)


# In[29]:


then=datetime.now()
results = pd.DataFrame()
for stock_name in stock_names :
    print(stock_name)
    
    trades_times=[]
    bid_times=[]
    ask_times=[]
    trades_rounds=0
    volume_rounds=0
    prev_trade_time=np.nan
    prev_bid_time=np.nan
    prev_ask_time=np.nan
    spreads=[]
    trades_price=[]
    prev_date=''
    
    for chunk in pd.read_csv("scandi.csv", engine='python',header=None, chunksize=500000, usecols=[0,2,3,4,7,8,10,11,14,15], skip_blank_lines=False) :        
        chunk.columns = ['stock name','bid','ask','trade','trade vol','update','date','time','code' ]

        for name,bid,ask,trade,trade_vol,update,date,time,code in chunk.to_numpy():
#  stock identifier  & condition code== XT & condition code==nan & bid price > ask price & (if it's a new date don't add time)
            if (name== stock_name) & ((code == 'XT') | (str(code)=='nan'))& (ask>bid) & (date==prev_date) :
                if update ==1 :
                        
                    if (np.isnan(prev_trade_time)==False) :
                        trades_times.append(time-prev_trade_time)
                    prev_trade_time=time
                        
                    trades_price.append(trade)
                    spreads.append(ask-bid)
                        
                    if trade % 10 == 0:
                        trades_rounds+=1
                    if trade_vol % 10 == 0:
                        volume_rounds+=1
                        
                if update ==2 :
                    if (np.isnan(prev_bid_time)==False) :
                        bid_times.append(time-prev_bid_time)
                    prev_bid_time=time
                        
                if update ==3 :
                    if (np.isnan(prev_ask_time)==False) :
                        ask_times.append(time-prev_ask_time)
                    prev_ask_time=time
                    
            prev_date=date
            
    if not trades_times:
        trades_times = [np.nan]
    if not bid_times:
        bid_times=[np.nan]
    if not ask_times:
        ask_times=[np.nan]
    if not spreads:
        spreads=[np.nan]
    if not trades_price:
        trades_price=[np.nan]
                
#   Stock code / Mean time between trades / Median time between trades / Mean time between tick changes / Median time between tick changes / Longest time between trades / Longest time between tick changes / Mean bid ask spread / Median bid ask spread / Examples of the round number effect
    r=[stock_name,st.mean(trades_times),st.median(trades_times),st.mean(bid_times),st.median(bid_times),st.mean(ask_times),st.median(ask_times), max(trades_times),max(bid_times),max(ask_times), st.mean(spreads),st.median(spreads), (trades_rounds/len(trades_price)),(volume_rounds/len(trades_price))]
    results= results.append(pd.DataFrame(r).T)
            
            
results.columns= ['Stock','Mean time trades (s)', 'Median time trades (s)', 'Mean time bid (s)', 'Median time bid (s)', 'Mean time ask (s)', 'Median time ask (s)', 'Longest time trade (s)', 'Longest time bid (s)', 'Longest time ask', 'Mean spread', 'Median spread', 'Round number probability price (%)','Round number probability volume (%)']
results =results.reset_index(drop=True)

now=datetime.now()
print('time elapsed : ',now-then)


# In[32]:


pd.set_option('display.max_rows', 100)
results





